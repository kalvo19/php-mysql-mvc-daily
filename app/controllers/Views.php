<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 10:53
 */

require_once('../app/libraries/SiteController.php');

class Views extends SiteController {

    public function index() {
        parent::view('index');
    }
}
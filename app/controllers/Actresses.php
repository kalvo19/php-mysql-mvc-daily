<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 11:19
 */

require_once('../app/libraries/SiteController.php');

class Actresses extends SiteController {

    private $actressModel = '';

    public function listar() {
        $this->actressModel = parent::model('Actress');
        $actresses = $this->actressModel->selectAll();
        $data = [
            'actresses' => $actresses
        ];

        $this->view('actresses', $data);
    }
}
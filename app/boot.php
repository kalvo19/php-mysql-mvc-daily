<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 19/03/2019
 * Time: 19:54
 */

// -- SLIDE [app > config > config.php] --

// Carga el fichero 'config/config.php' una sola vez, verificando que
// no se incluya de nuevo.
require_once('../app/config/config.php');

// Carga el fichero 'config/config.php' una sola vez, verificando que
// no se incluya de nuevo.
require_once('../app/libraries/Core.php');

// Carga el fichero 'libraries/Database.php' una sola vez, verificando que
// no se incluya de nuevo.
require_once('../app/libraries/Database.php');
<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 19/03/2019
 * Time: 20:21
 */

/*
 * Fichero donde se establecerán los parámetros de configuración tanto del servidor web
 * como el servidor de la base de datos.
 */


// Ruta del directorio 'root' del servidor (normalmente localhost/nombreDelProyecto)
define('URL_ROOT_PATH', 'http://localhost/php-mysql-mvc-daily');
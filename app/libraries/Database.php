<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 19/03/2019
 * Time: 20:28
 */

/*
 * Contiene la libreria de métodos que se van a utilizar para hacer cualquier tipo
 * de consultas a la base de datos (SELECT, INSERT INTO, UPDATE, DELETE). Es la clase padre
 * de todos los modelos que estan ubicados en el directorio '/app/models/'
 */

class Database {

    // ------------- PROPIEDADES DE LA CLASE ------------- //

    // Contiene una única connexión a la base de datos (objeto MySqli)
    private $connection = '';

    // Debe contener el nombre del servidor de la base de datos
    //(en LAPP XAMPP, WAMP, LAMP es igual localhost)
    private $servername = 'localhost';

    // Debe contener el nombre de la base de datos a la que se va a conectar
    private $dbname = 'mvc-php-daily';

    // Debe contener el nombre del usuario que se va a conectar a la base de datos
    private $username = 'kalvo19';

    // Debe contener la constraseña del usuario que se va a conectar a la base de datos
    private $password = '123456';

    // Contiene los mensajes de errores lanzados a la hora de crear una connexión
    // con la base de datos o bien hacer una consulta en ella
    private $error = '';


    // ----------------- CONSTRUCTOR ----------------- //

    public function __construct() {
        try {

            // Abre una conexión al servidor de MySQL
            $this->connection = mysqli_connect($this->servername, $this->username, $this->password, $this->dbname);

            /*
             * Otra manera de llamar a la función mysqli_connect es mediante la llamada al constructor de
             * la clase Mysqli que PHP tiene por defecto integrado.
             */
            // $this->connection = mysqli::__construct($this->host, $this->username, $this->password, $this->dbname);

        } catch (Exception $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }


    // ------------- MÉTODOS DE LA CLASE ------------- //

    public function query($sql) {
        $data = [];

        try {
            $queryResults = mysqli_query($this->connection, $sql);

            /*
             * Otra manera de llamar a la función 'mysqli_query()' es mediante la llamada al método estático
             * la clase Mysqli que PHP tiene por defecto integrado.
             */
            //$queryResults = mysqli::query($sql);

            while ($row = $queryResults->fetch_object()) {
                array_push($data, $row);
            };
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }

        $this->close();

        return $data;
    }

    public function close() {
        mysqli_close($this->connection);
    }

}
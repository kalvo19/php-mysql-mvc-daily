<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 19/03/2019
 * Time: 22:16
 */

class Core {

    // ------------- PROPIEDADES DE LA CLASE ------------- //

    // Contiene el controlador que se esta utilizando actualmente
    private $currentController = 'Views';

    // Contiene el método del controlador que se esta utilizando actualmente
    private $currentMethod = 'index';

    // Contiene los parámetros que van a ser pasados al método del controlador
    // que se esta utilizando actualmente
    private $parameters = [];


    // ----------------- CONSTRUCTOR ----------------- //

    public function __construct() {

        // Devuelve un vector con la url trozeada.
        //
        // $url[0] = controlador
        // $url[1] = método de ese controlador
        // $url[2] = parametros del método de ese controlador
        $url = $this->getURL();

        // Comprueba si existe el controlador solicitado en el directorio '/controllers'
        // a través de la URL y si existe, reemplaza el valor del atributo 'currentController'
        // por el nombre del controlador solicitado.
        if (file_exists('../app/controllers/' . $url[0] . '.php')) {
            $this->currentController = $url[0];

            // Una vez obtenido el nombre del controlador, se elimina del vector de donde lo hemos
            // recogido.
            unset($url[0]);
        }

        // Se incluye la clase del controlador solicitado para así poder instanciarlo y acceder
        // a sus métodos.
        require_once('../app/controllers/' . $this->currentController . '.php');

        // Se instancia la clase controlador solicitada y se guarda en el atributo 'currentController'
        // desde donde se podrá llamar a sus métodos.
        $this->currentController = new $this->currentController;

        // Se comprueba si en la URL solicitada hay un segundo elemento, que en este caso corresponde
        // a uno de los métodos disponibles en el controlador incluido anteriormente.
        if (isset($url[1])) {

            // Comprueba si finalmente ese método existe en el controlador actual cargado, y si es
            // así reemplaza el valor existente en 'currentMethod' por el nombre del método que se
            // ha solicitado.
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];

                // Una vez obtenido el nombre del método, se elimina del vector de donde lo hemos
                // recogido.
                unset($url[1]);
            }
        }

        // Se comprueba si se añadido un tercer elemento en la URL. Este tercer elemento
        // corresponden a los parámetros o argumentos que se van a pasar al método solicitado
        // anteriormente, perteneciente a la clase solicitada en $url[0]. En caso que no exista
        // este tercer elemento, se dejará un vector vacío.
        $this->parameters = $url ? array_values($url) : [];

        // Por último, con la función 'call_user_func_array()' de PHP 4.0.2 llamamos al método
        // solicitado que pertenece a la clase que contiene el controlador solicitado, con un vector
        // de parámetros que se hayan enspecificado en la URL.
        //
        // Ej: call_user_func_array([claseControlador, métodoDelControlador], parámetrosDelMetodo)
        call_user_func_array([$this->currentController, $this->currentMethod], $this->parameters);
    }


    // ------------- MÉTODOS DE LA CLASE ------------- //

    /*
     *
     */
    public function getURL() {

        // Comprueba si se ha solicitado una nueva URL en la barra de direcciones, en caso de
        // seguir con el directorio root (localhost/nombreDelProyecto/) no entrará en la condición
        //
        // Ej: localhost/miproyecto/controlador = TRUE (Entra en el if)
        // Ej: localhost/miproyecto/ = FALSE (No entra en el if)
        if (isset($_GET['url'])) {
            // La función 'rtrim()' elimina o recorta el cáracter pasado como segundo argumento
            // del final de la cadena de texto pasada como primer argumento
            //
            // Ejemplo: localhost/stifler/come/back/ =====> localhost/stifler/come/back
            $url = rtrim($_GET['url'], '/');

            // Sanea la url pasada como primer y único parámetro
            $url = $this->sanitizeURL($url);

            // Parte la URL en tres partes y las guarda en un array o vector
            $url = explode('/', $url);
            return $url;
        }
    }


    /*
     * Elimina todos los caracteres excepto letras, dígitos y $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=
     * de la URL pasada como parámetro.
     */
    public function sanitizeURL($url) {
        // Filtra la variable pasado como primer parámetro utilizando el filtro pasado como segundo.
        return filter_var($url, FILTER_SANITIZE_URL);

        // NOTA: Estos filtros vienen predefinido en PHP, por lo que solo hace falta invocarlos
        // por el nombre. Existen filtros para comprobar correos electrónicos, direcciones MAC,
        // booleanos, floats (decimales), etc...
        //
        // Para ver más acerca de los filtros existentes véase:
        // http://php.net/manual/es/filter.filters.php
    }
}
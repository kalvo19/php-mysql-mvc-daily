<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 10:51
 */

/*
 * Fichero que contiene la clase padre de todos los controladores de la aplicación ubicados
 * en '/app/controllers'. Se encarga de hacer instancias a los modelos y de cargar las vistas
 * que el resto de controladores solicitarán en función de la URL enviada por el servidor.
 */

abstract class SiteController {

    // Retorna un objeto de la clase modelo pasada como parámetro en el método
    //
    // La variable '$model' debe ser una cadena de texto con el nombre de la clase
    // modelo que se quiera instanciar.
    public function model($model) {

        // Comprueba si existe una clase modelo con el nombre pasado en '$model'
        if (file_exists('../app/models/' . $model . '.php')) {
            require_once('../app/models/' . $model . '.php');
        }

        // Retorna la clase instanciada
        return new $model;
    }

    // Inlcluye la página HTML pasada como primer parámetro para que pueda ser rellenada con los
    // datos pasadas como segundo parámetro.
    //
    // La variable '$view' debe ser una cadena de texto con el nombre de la página que se quiera
    // mostrar al usuario.
    //
    // La variable '$data' pasada como segundo parámetro, suele equivaler a los datos recogidos
    // en la base de datos que enviamos al HTML para que estos sean mostrados de manera más estética.
    // No es obligatorio este parámetro.
    public function view($view, $data = []) {

        // Comprueba que la página que se desea cargar existe en el directorio 'view/pages' y la carga
        // para que el usuario pueda visualizarla en su navegador.
        if (file_exists('../app/views/pages/' . $view . '.php')) {
            include_once('../app/views/pages/' . $view . '.php');
        }
    }
}
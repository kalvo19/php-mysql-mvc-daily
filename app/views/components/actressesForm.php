<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 11:28
 */
?>
<div class="container-fluid">
    <div class="jumbotron index-page">
        <h1 class="title-page">Ver mi lista de actrices</h1>
        <div class="row justify-content-center">
            <form action="Actresses/listar" method="post">
                <button type="submit" name="actressList" class="btn btn-success submit-button">Descúbrelas</button>
            </form>
        </div>
    </div>
</div>

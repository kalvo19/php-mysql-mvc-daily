<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 14:07
 */
?>

<div class="container-fluid">
    <div class="jumbotron title-page"><h1>Mi lista de Actrices</h1></div>
<?php
    $row = 0;
    for ($i = 0; $i < count($data['actresses']); $i++) {

        if ($row % 3 == 0 && $row != 0) {
?>
    </div>
<?php
        }
        if ($row % 3 == 0 || $row == 0) {
?>
    <div class="row justify-content-center list-item">
<?php
        }
?>
        <div class="card col-xl-3 col-sm-3 element-listed m-4">
            <img class="card-img-top" src="<?php echo URL_ROOT_PATH . $data['actresses'][$i]->imagen; ?>" alt="Foto retrato de <?php echo $data['actresses'][$i]->fullName; ?>">
            <div class="card-body item-body-description">
                <h5 class="card-title"><?php echo $data['actresses'][$i]->fullName; ?></h5>
                <p class="card-text"><strong>Filmografía: </strong><?php echo $data['actresses'][$i]->filmography; ?></p>

                <div class="awards-count">
                    <h6>Premios</h6>
                    <?php
                        for ($y = 0; $y < $data['actresses'][$i]->oscars; $y++) {

                    ?>
                           <span class="oscar-award"><img src="http://localhost/php-mysql-mvc-daily/public/assets/images/oscar.png" alt="Fotos del premio Oscar"></span>
                    <?php
                        }

                    for ($y = 0; $y < $data['actresses'][$i]->goldenGlobes; $y++) {

                        ?>
                        <span class="oscar-award"><img src="http://localhost/php-mysql-mvc-daily/public/assets/images/gold-globe.png" alt="Fotos del premio Oscar"></span>
                        <?php
                    }

                    ?>
                </div>
            </div>
        </div>

<?php

        $row++;
    }
?>

</div>

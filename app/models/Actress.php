<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 23/03/2019
 * Time: 11:55
 */

/*
 * Clase modelo que recogerá todos los métodos y propiedades relativos a la gestión
 * de los datos de las 'Actrices' almacenadas en la base de datos.
 */

class Actress extends Database {

    private $fullName = '';
    private $filmography = '';
    private $age = '';
    private $oscars;
    private $goldenGlobes;

    public function __construct() {
        parent::__construct();
    }

    // Devuelve una lista con todas las actrices almacenadas en la base de datos
    public function selectAll() {
        // Guarda la consulta que se realizará en el servidor SQL
        $sql = "SELECT * FROM actresses;";

        // Ejecuta la consulta y guarda el resultados en '$data'
        $data = $this->query($sql);

        // Retorna los resultados
        return $data;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 19/03/2019
 * Time: 19:41
 */

/*
* --------------------------------- INICIO DE LA APLICACIÓN ---------------------------------
*
* Fichero: index.php
*
* Incluye el archivo 'boot.php' que se encarga a su vez de incluir el modelo de la base de
* datos 'model/Database.php' y el controlador general de la aplicación
* 'libraries/Controller.php'.
*/

// -- SLIDE [require and include] --

// Carga el fichero 'boot.php' una sola vez, verificando que no se incluya de nuevo
require_once('../app/boot.php');

// Instancia la clase 'libraries/Core.php'
$inicio = new Core();

